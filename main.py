from flask import jsonify, abort
#from firebase_admin import auth
#from functools import wraps
from gensim.models import KeyedVectors

wv = None

def json_abort(status_code, message):
    data = {
        'error': {
            'code': status_code,
            'message': message
        }
    }
    response = jsonify(data)
    response.status_code = status_code
    abort(response)

# def firebase_auth_required(f):
#     @wraps(f)
#     def wrapper(request):
#         authorization = request.headers.get('Authorization')
#         id_token = None
#         if authorization and authorization.startswith('Bearer '):
#             id_token = authorization.split('Bearer ')[1]
#         else:
#             json_abort(401, message="Invalid authorization")

#         try:
#             decoded_token = auth.verify_id_token(id_token)
#         except Exception as e: # ValueError or auth.AuthError
#             json_abort(401, message="Invalid authorization")
#         return f(request, decoded_token)
#     return wrapper

'''
Input: list of words
Output: list of reversed-plural versions of input words
'''
def get_plural_variants(input_words):
    if type(input_words) != list:
        return []
    alt_words = []
    for word in input_words:
        if word[-3:] == "ies":
            alt_words.append(word[:-3] + 'y')
        elif word[-1] == 'y':
            alt_words.append(word[:-1] + "ies")
        elif word[-3:] == 'oes':
            alt_words.append(word[:-2])
        elif word[-2:] == 'eo':
            alt_words.append(word + 's')
        elif word[-1] == 'o':
            alt_words.append(word + 'es')
        elif word[-1] == 's':
            alt_words.append(word[:-1])
        else:
            alt_words.append(word + 's')
    return alt_words

'''
Input: list of words
Output: list of words with inverted capitalization
'''
def get_case_variants(input_words):
    if type(input_words) != list:
        return []
    alt_words = []
    for word in input_words:
        new_word = (word[0].lower() if word[0].isupper() else word[0].upper()) + word[1:]
        if new_word not in input_words:
            alt_words.append(new_word)
    return alt_words

#@firebase_auth_required
def generate_ideas(request, decoded_token = None):
    data = request.json.get('data')
    if 'allWords' not in data:
        json_abort(400, message="Missing input")
    all_words = data['allWords'] # a comma delimited string
    words = all_words.split(",") # convert comma delimited string to list

    # For each word, get the reversed plural version (e.g., 'dog' becomes 'dogs')
    #pv = get_plural_variants(words)
    #words.extend(pv)
    # For each word, get the case-insensitive version (e.g., 'dog' and 'Dog')
    cw = get_case_variants(words)
    words.extend(cw)

    # Load model as KeyedVector
    global wv
    if not wv:
        wv = KeyedVectors.load_word2vec_format("med-google", binary=True)

    # Remove out-of-vocab words
    for word in words:
        if not wv.has_index_for(word):
            words.remove(word)
    
    # Get similarities
    result = []
    try:
        similarities = wv.most_similar(words, topn=10) # returns array of tuples
        for tup in similarities:
            new_word = tup[0].replace("_", " ") # remove unnecessary underscores
            # If word is plural, get non-plural version - and vice versa
            plural_variants = get_plural_variants([new_word])
            # Get case variants
            case_variants = get_case_variants([new_word] + plural_variants)
            # Gather the new word and all its variants into one list
            new_list = case_variants + plural_variants + [new_word]
            # Add new word to result so long as it & its variant do not appear in the user input or current result
            in_input = any(item in new_list for item in words)
            in_result = any(item in new_list for item in result)
            # If both checks pass, add to result
            if not in_input and not in_result:
                result.append(new_word)
    except:
        result = []

    return jsonify({
        'data': {
            'result': result
        }
    })