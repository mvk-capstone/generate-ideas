Flask==1.0.2
gensim==4.0.1
firebase-admin==4.5.3
python-Levenshtein==0.12.2
pyparsing