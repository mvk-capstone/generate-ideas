import unittest
from main import (get_plural_variants, get_case_variants)

class TestPluralVariants(unittest.TestCase):
    def test_s(self):  # sourcery skip: class-extract-method
        """
        Test that it can swap plurals with 's', or singulars without 's'
        """
        data = ['truck', 'cars']
        result = get_plural_variants(data)
        self.assertListEqual(result, ['trucks', 'car'])

    def test_y_ies(self):
        """
        Test that it can create plurals with 'ies' and remove 'ies' in plurals.
        """
        data = ['stickies', 'bunny']
        result = get_plural_variants(data)
        self.assertListEqual(result, ['sticky', 'bunnies'])

    def test_o_oes(self):
        """
        Test that it can create plurals with 'o' and remove 'ies' in plurals.
        """
        data = ['tomato', 'potatoes', 'rodeo', 'rodeos', 'hero']
        result = get_plural_variants(data)
        self.assertListEqual(result, ['tomatoes', 'potato', 'rodeos', 'rodeo', 'heroes'])

    def test_mixed(self):
        """
        Test that it can handle many types of words
        """
        data = ['driver', 'ally', 'abilities', 'friends']
        result = get_plural_variants(data)
        self.assertListEqual(result, ['drivers', 'allies', 'ability', 'friend'])
    
    def test_empty_list(self):
        """
        Test that it can handle an empty list
        """
        data = []
        result = get_plural_variants(data)
        self.assertListEqual(result, [])

    def test_is_list(self):
        """
        Test that it gracefully fails for improper data types
        """
        data = 20  # int
        result = get_plural_variants(data)
        self.assertListEqual(result, [])

class TestCaseVariants(unittest.TestCase):
    def test_invert(self):
        """
        Test that it can invert case on numerous words
        """
        data = ['Truck', 'car']
        result = get_case_variants(data)
        self.assertListEqual(result, ['truck', 'Car'])

    def test_duplicates(self):
        """
        Test that it doesn't convert words if the converted version exists already
        """
        data = ['truck', 'Truck', 'car']
        result = get_case_variants(data)
        self.assertListEqual(result, ['Car'])

    def test_empty_list(self):
        """
        Test that it can handle an empty list
        """
        data = []
        result = get_case_variants(data)
        self.assertListEqual(result, [])

    def test_is_list(self):
        """
        Test that it gracefully fails for improper data types
        """
        data = 20  # int
        result = get_case_variants(data)
        self.assertListEqual(result, [])

if __name__ == '__main__':
    unittest.main()